# How to use: 2019_07_29_LegacyTextureAndSprite   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.legacytextureandsprite":"https://gitlab.com/eloistree/2019_07_29_LegacyTextureAndSprite.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.legacytextureandsprite",                              
  "displayName": "LegacyTextureAndSprite",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Project to project unity package design to store cool texture and sprite that I did in the past and that I want to keep just in case it could be handy.",                         
  "keywords": ["Script","Tool","Legacy","Assets","Texture","Sprite"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    